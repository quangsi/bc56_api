// đồng bộ và bất đồng bộ
// setTimeout => bất đồng bộ
// đồng bộ chạy hết , sau đó mới tới bất động bộ
// event loop :https://www.loginradius.com/blog/static/68b5a28f6bdca97b73593056ae425a8d/21b4d/event_loop_illustration.png

setTimeout(function () {
  console.log(4);
}, 1000);
setTimeout(function () {
  console.log(5);
}, 0);

console.log(1);
console.log(2);
console.log(3);

// axios

var api_link = "https://api.tiki.vn/raiden/v2/menu-config?platform=desktop";
axios({
  url: api_link,
  method: "GET",
})
  .then(function (res) {
    console.log("🚀 - .then - res", res.data.menu_block.items);
    // success
  })
  .catch(function (err) {
    console.log("🚀 - err", err);
  });

// state : pendding, resolve (sucess), reject (fail)
