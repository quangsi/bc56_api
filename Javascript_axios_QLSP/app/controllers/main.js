// https://633ec05b0dbc3309f3bc5455.mockapi.io/:endpoint

function fetchProductList() {
  batLoading();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "GET",
  })
    .then(function (res) {
      renderDSSP(res.data);
      tatLoaing();
    })
    .catch(function (err) {
      tatLoaing();
      console.log("🚀 - err", err);
    });
}
fetchProductList();
// crud
// array 10 item => 10 thẻ tr

function xoaSp(id) {
  batLoading();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại api  lấy tất cả sp trên server về sau khi xoá thành công
      fetchProductList();

      console.log("😀 - xoaSp - res", res);
    })
    .catch(function (err) {
      tatLoaing();
    });
}

function themSp() {
  var sp = layThongTinTuForm();
  batLoading();
  axios({
    url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
    method: "POST",
    data: sp,
  })
    .then(function (res) {
      console.log("😀 - themSp - res", res);
      // tắt modal
      $("#myModal").modal("hide");
      // lấy danh sách mới nhất từ server
      fetchProductList();
    })
    .catch(function (err) {
      tatLoaing();
      console.log("😀 - themSp - err", err);
    });
}

// edit :1) lấy chi tiết đối tượng cần sửa ( get by id), 2) đưa dữ liệu mới nhất lên server ( put)
function suaSp(id) {
  // lấy chi tiết sản phẩm dựa vào id
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${id}`,
    method: "GET",
  })
    .then(function (res) {
      $("#myModal").modal("show");
      showThonTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoaing();
    });
}
function capNhatSp() {
  console.log("yes");
  var sp = layThongTinTuForm();
  axios({
    url: `https://633ec05b0dbc3309f3bc5455.mockapi.io/product/${sp.id}`,
    method: "PUT",
    data: sp,
  })
    .then(function (res) {
      // lấy lại dssp sau khi cập nhật thành công
      fetchProductList();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}
function reset() {
  // productForm ~ id của thẻ form chứa các input cần reset
  document.getElementById("productForm").reset();
}
